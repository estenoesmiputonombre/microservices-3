
# Volumes

Volumes dont dependent of the structure of the host, they are all fully managed by docker.

* Volumes are _easier_ to **back up** or **migrate** than bind mounts.

* You can manage volumes using **Docker CLI** commands or the **Docker API**.

* Volumes work on both __linux__ and _windows_ containers.

* Volumes can be more *safely* shared among multiple containers

* Volumes drivers let you store volumes on **remote hosts** or **cloud providers**, to encrypt the contents of volumes, or to add other funcionality.

* New volumes can have their content pre-populated by a container.

* Volume *doesnt* increase the **size** of the container using it.


#### Notes:

If your container generates non-persistent state data, consider using **tmpfs mount** to:

* *avoid* storing the dara anywhere permanently
* *increase* the container's performance by avoiding writing into the containers writable layer.

Volumes use **rprivate** bind propagation, and it is not configurable for volumes.
